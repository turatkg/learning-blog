<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Language Lines
    |--------------------------------------------------------------------------
    |
    | Website specific translations
    |
    */

    'posts' => 'Посттор',

    'main' => 'Башкы',
    'main_page' => 'Башкы бет',

    'recent_news' => 'Акыркы кабарлар',

    'new_post' => 'Жаңы пост кошуу',
    'all_posts' => 'Бардык посттор',

    'title' => 'Аталышы',

    'confirm_delete' => 'Чын эле өчүргүңүз келип атабы?',


    'incorrect_password_or_email' => 'Сырсөз же email тура эмес берилди',
    'you_are_not_activated_yet' => 'Сиздин аккаунт дагы активация боло элек',
    'is_editors_pick' => 'Автордун тандоосу',

    'error_not_found' => 'Баракча табылган жок',
    'page_you_are_looking_for_not_found' => 'Сиз карап жаткан бет табылган жок',

    'page_you_are_looking_for_forbidden' => 'Сиз карап жаткан бетти ачууга тыйуу салынган',

    'rate_this_post' => 'Бул постту баалаңыз',

    'value_not_provided' => 'Маани берилген жок',

    'successfully_rated' => 'Бааңыз ийгиликтүү сакталды',
    'already_rated' => 'Кайра баа берүү мүмкүн эмес',

    'error' => 'Ката',
    'success' => 'Ийгилик',

    'not_rated' => 'Баа берилген эмес',


];
