<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Language Lines
    |--------------------------------------------------------------------------
    |
    | Website specific translations
    |
    */

    'posts' => 'Посты',

    'main' => 'Главная',
    'main_page' => 'Главная страница',

    'recent_news' => 'Последние новости',

    'new_post' => 'Добавить новость',
    'all_posts' => 'Все новости',

    'title' => 'Название',

    'confirm_delete' => 'Вы действительно хотите удалить?',


    'incorrect_password_or_email' => 'Пароль или почта неправильная',
    'is_editors_pick' => 'Выбор редакции',

    'error_not_found' => 'Страница не найдена',
    'page_you_are_looking_for_not_found' => 'Страница, которую вы ищете не может быть найдена',

    'rate_this_post' => 'Оцените этот пост',

    'value_not_provided' => 'Значение не задано',

    'successfully_rated' => 'Ваша оценка успешно принята',
    'already_rated' => 'Нельзя оценивать дважды',
    'error' => 'Ошибка',
    'success' => 'Успешно',
 
    'not_rated' => 'Нет оценок',


];
