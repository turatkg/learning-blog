<li class="comment">
    <div class="vcard bio">
        <img src="{{asset($comment->author->image)}}" alt="Image placeholder">
    </div>
    <div class="comment-body">
        <h3>{{$comment->author->name}} </h3>
        <div class="meta">{{$comment->created_at->diffForHumans()}} </div>
        <h3>{{$comment->title}} </h3>
        <p>{{$comment->message}} </p>
        <p><a href="#" class="reply reply-to-comment" data-comment_id='{{$comment->id}}'>Reply</a></p>

    </div>

    @if($comment->replies->count())
    <ul class="children">
        @foreach($comment->replies as $comment)
        @include('web.post.comment', $comment)
        @endforeach
    </ul>
    @endif

</li>
