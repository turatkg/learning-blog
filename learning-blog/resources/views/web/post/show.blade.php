@extends('web.layouts.base')

@section('title', $row->title)

@section('content')

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 single-content">

                <p class="mb-5">
                    <img src="{{asset($row->image)}}" alt="Image" class="img-fluid w-100">
                </p>
                <h1 class="mb-4">
                    {{$row->title}}
                </h1>
                <div class="post-meta d-flex mb-5">
                    <div class="bio-pic mr-3">
                        <img src="{{asset($row->author->image)}}" alt="Image" class="img-fluidid">
                    </div>
                    <div class="vcard">
                        <span class="d-block">
                            <a href="#">{{$row->author->name}} </a> in <a href="#">{{$row->category->title}} </a>
                        </span>
                        <span class="date-read">
                            {{$row->created_at->diffForHumans()}}
                            <span class="mx-1">&bullet;</span>
                            3 min read
                            <span class="icon-star2"></span>
                            {{$row->total_score}}
                            <span class="mx-1">&bullet;</span>
                            <span class="icon-eye"></span>
                            {{$row->views}}
                        </span>
                    </div>
                </div>

                {!! $row->content !!}

                <div class="pt-5">
                    <p>Categories: <a href="#">Design</a>, <a href="#">Events</a> Tags: <a href="#">#html</a>, <a
                            href="#">#trends</a></p>
                </div>


                <div class="pt-5">
                    <h2>{{trans('t.rate_this_post')}}: </h2>

                    @auth
                    <form id="rate-form" method="POST">
                        <div class="form-group">
                            <label class="pr-3">
                                1
                                <input type="radio" name="value" value="1">
                            </label>
                            <label class="pr-3">
                                2
                                <input type="radio" name="value" value="2">
                            </label>
                            <label class="pr-3">
                                3
                                <input type="radio" name="value" value="3">
                            </label>
                            <label class="pr-3">
                                4
                                <input type="radio" name="value" value="4">
                            </label>
                            <label class="pr-3">
                                5
                                <input type="radio" name="value" value="5">
                            </label>
                        </div>
                        <button class="btn btn-primary" type="submit">{{trans('t.rate')}} </button>
                    </form>
                    @else
                    <div class="comment-form-wrap pt-5">
                        <h3>{{trans('t.login_to_rate')}} </h3>
                        <a href="{{route('login')}} " class="btn btn-success">{{trans('t.login')}} </a>
                    </div>
                    @endauth
                </div>

                <div class="pt-5">
                    <div class="section-title">
                        <h2 class="mb-5">{{$row->allComments->count()}} {{trans('t.comments')}} </h2>
                    </div>
                    <ul class="comment-list">

                        @foreach($row->comments as $comment)
                        @include('web.post.comment', $comment)
                        @endforeach

                    </ul>
                    <!-- END comment-list -->

                    @auth
                    <div class="comment-form-wrap pt-5">
                        <div class="section-title">
                            <h2 class="mb-5">Leave a comment</h2>
                        </div>
                        <form action="{{route('web.post.comment', $row->id)}}" method="POST" class="p-5 bg-light">
                            @csrf
                            @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}} </li>
                                @endforeach
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="name">{{trans('t.title')}} *</label>
                                <input type="text" class="form-control" id="name" name="title">
                            </div>

                            <div class="form-group">
                                <label for="message">{{trans('t.message')}} </label>
                                <textarea name="message" id="message" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Post Comment" class="btn btn-primary py-3">
                            </div>

                        </form>
                    </div>
                    @else
                    <div class="comment-form-wrap pt-5">
                        <h3>{{trans('t.login_to_post_comment')}} </h3>
                        <a href="{{route('login')}} " class="btn btn-success">{{trans('t.login')}} </a>
                    </div>
                    @endauth

                </div>
            </div>


            <div class="col-lg-3 ml-auto">
                <div class="section-title">
                    <h2>Popular Posts</h2>
                </div>

                <div class="trend-entry d-flex">
                    <div class="number align-self-start">01</div>
                    <div class="trend-contents">
                        <h2><a href="blog-single.html">News Needs to Meet Its Audiences Where They Are</a></h2>
                        <div class="post-meta">
                            <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">News</a></span>
                            <span class="date-read">Jun 14 <span class="mx-1">&bullet;</span> 3 min read <span
                                    class="icon-star2"></span></span>
                        </div>
                    </div>
                </div>

                <div class="trend-entry d-flex">
                    <div class="number align-self-start">02</div>
                    <div class="trend-contents">
                        <h2><a href="blog-single.html">News Needs to Meet Its Audiences Where They Are</a></h2>
                        <div class="post-meta">
                            <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">News</a></span>
                            <span class="date-read">Jun 14 <span class="mx-1">&bullet;</span> 3 min read <span
                                    class="icon-star2"></span></span>
                        </div>
                    </div>
                </div>

                <div class="trend-entry d-flex">
                    <div class="number align-self-start">03</div>
                    <div class="trend-contents">
                        <h2><a href="blog-single.html">News Needs to Meet Its Audiences Where They Are</a></h2>
                        <div class="post-meta">
                            <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">News</a></span>
                            <span class="date-read">Jun 14 <span class="mx-1">&bullet;</span> 3 min read <span
                                    class="icon-star2"></span></span>
                        </div>
                    </div>
                </div>

                <div class="trend-entry d-flex pl-0">
                    <div class="number align-self-start">04</div>
                    <div class="trend-contents">
                        <h2><a href="blog-single.html">News Needs to Meet Its Audiences Where They Are</a></h2>
                        <div class="post-meta">
                            <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">News</a></span>
                            <span class="date-read">Jun 14 <span class="mx-1">&bullet;</span> 3 min read <span
                                    class="icon-star2"></span></span>
                        </div>
                    </div>
                </div>

                <p>
                    <a href="#" class="more">See All Popular <span class="icon-keyboard_arrow_right"></span></a>
                </p>
            </div>


        </div>

    </div>
</div>
@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

    $("#rate-form").on('submit', function(e){
        e.preventDefault();
        var score = $('#rate-form input[name=value]:checked').val();

        if(score){
            $.ajax({
                method: "POST",
                url: "{{route("web.post.rate", $row->id)}}",
                data: {
                    _token: "{{csrf_token()}}",
                    value: score
                },
                success: function(res){
                    swal("{{trans('t.success')}}", res.message, 'success');
                },
                error: function(error){
                    swal("{{trans('t.error')}}", JSON.parse(error.responseText).message, 'error');
                }
            });
        }else{
            alert("{{trans("t.value_not_provided")}}");
        }
    });

    function generateCommentForm(id){
        return `<div class="comment-form-wrap pt-1 reply-form">
                <div class="">
                    <h3 class="">Reply a comment</h3>
                    <button class="btn btn-sm btn-dark float-right remove-reply-form" >x</button>
                </div>
                <form action="{{route('web.post.comment', $row->id)}}" method="POST" class="p-3 bg-light">
                    @csrf
                    <input type="hidden" name="parent_comment_id" value='${id}'>
                    <div class="form-group">
                        <label for="name">{{trans('t.title')}} *</label>
                        <input type="text" class="form-control" id="name" name="title">
                    </div>

                    <div class="form-group">
                        <label for="message">{{trans('t.message')}} </label>
                        <textarea name="message" id="message" cols="30" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Post Comment" class="btn btn-primary py-3">
                    </div>

                </form>
            </div>`;
    }

    $(document).on('click', '.reply-to-comment', function(e){
        e.preventDefault();
        var button = $(this)[0];
        var buttonContainer = $(button).closest('.comment-body')[0];

        if($(buttonContainer).children('.reply-form').length == 0){
            var commentId = $(button).data('comment_id');
            $(buttonContainer).append(generateCommentForm(commentId));
        }
    });

    $(document).on('click', '.remove-reply-form', function(e){
        var container = $(this).closest('.reply-form');
        if(container.length){
            container[0].remove();
        }
    });


</script>

@endsection
