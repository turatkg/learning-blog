<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title', 'Пайдалуу блог') </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css?family=B612+Mono|Cabin:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{mix('/assets/css/dist.css')}}">
    <link rel="stylesheet" href="{{mix('/assets/css/app.css')}}">
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <div class="site-wrap">

        @include('web.layouts.header')

        @yield('content')

        @include('web.layouts.footer')


    </div>
    <!-- .site-wrap -->

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#ff5e15" /></svg></div>

    <script src="{{mix('/assets/js/app.js')}}"></script>

    @yield('scripts')

</body>

</html>
