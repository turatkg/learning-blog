@extends('admin.auth.base')

@section('content')
<p class="login-box-msg">{{trans('t.register')}} </p>

<form action="{{route('register-user')}}" method="post" enctype="multipart/form-data">
    @csrf
    @if(session()->has('message'))
    <div class="alert alert-danger">
        {{session()->get('message')}}
    </div>
    @endif
    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </div>
    @endif
    <div class="input-group mb-3">
        <input type="text" name="name" class="form-control" placeholder="Name">
    </div>
    <div class="input-group mb-3">
        <input type="text" name="last_name" class="form-control" placeholder="Last name">
    </div>
    <div class="input-group mb-3">
        <input type="file" accept="image/*" name="image" class="form-control">
    </div>
    <div class="input-group mb-3">
        <input type="email" name="email" class="form-control" placeholder="Email">
    </div>
    <div class="input-group mb-3">
        <input type="password" name="password" class="form-control" placeholder="Password">
    </div>
    <div class="input-group mb-3">
        <input type="password" name="password_confirmation" class="form-control" placeholder="Password confirmation">
    </div>
    <div class="row">
        <div class="col-8">
        </div>
        <!-- /.col -->
        <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
        </div>
        <!-- /.col -->
    </div>
</form>

<div class="social-auth-links text-center mb-3">
    <p>- OR -</p>
    <a href="#" class="btn btn-block btn-primary">
        <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
    </a>
    <a href="#" class="btn btn-block btn-danger">
        <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
    </a>
</div>
<!-- /.social-auth-links -->

<p class="mb-1">
    <a href="forgot-password.html">I forgot my password</a>
</p>
<p class="mb-0">
    <a href="register.html" class="text-center">Register a new membership</a>
</p>
</div>
<!-- /.login-card-body -->
@endsection
