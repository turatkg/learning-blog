<script src="{{asset('/assets/libs/tinymce/tinymce.min.js')}}"></script>
<script>
    tinymce.init({
            selector: '.editor',
            menubar: '',
            plugins: 'preview importcss searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr nonbreaking toc insertdatetime lists wordcount imagetools textpattern noneditable quickbars emoticons',
            toolbar: 'undo redo | bold italic underline strikethrough | formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat |  emoticons | insertfile image media pageembed link codesample | preview fullscreen',
            image_advtab: true,
            importcss_append: true,
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: 'mceNonEditable',
            toolbar_mode: 'sliding',
            content_style: '.mymention{ color: gray; }',
            contextmenu: 'link image imagetools table configurepermanentpen',
        });
</script>
