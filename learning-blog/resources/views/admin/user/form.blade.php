<div class="card-body">
    <div class="form-group">
        <label>{{trans('t.name')}}</label>
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>{{trans('t.last_name')}}</label>
        {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>{{trans('t.email')}}</label>
        {!! Form::text('email', null, ['class' => 'form-control editor', 'rows' => 15]) !!}
    </div>
    <div class="form-group">
        <label>{{trans('t.password')}}</label>
        {!! Form::password('password', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>{{trans('t.password_confirmation')}}</label>
        {!! Form::password('password_confirmation', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>{{trans('t.role')}}</label>
        {!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label for="exampleInputFile">{{trans('t.image')}} </label>
        {!! Form::file('image', ['id' => 'exampleInputFile', 'accept' => 'image/*']) !!}
    </div>
    <div class="form-group">
        <div class="form-check">
            {!! Form::hidden('active', 0) !!}
            {!! Form::checkbox('active', 1, null, ['class' => 'form-check-input', 'id' => 'exampleCheck1']) !!}
            <label class="form-check-label" for="exampleCheck1">Активдүү</label>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-success">Сактоо</button>
    </div>
