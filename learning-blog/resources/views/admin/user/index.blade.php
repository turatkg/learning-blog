@extends('admin.layouts.base')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Колдонуучулар </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Башкы</a></li>
                        <li class="breadcrumb-item active">Колдонуучулар</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Бардык колдонуучулар</h3>
                            <div class="float-right">
                                <a href="{{route('admin.user.create')}}" class="btn btn-primary">Колдонуучу кошуу</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Аты</th>
                                        <th>Фамилиясы</th>
                                        <th>Эл дареги</th>
                                        <th>Ролу</th>
                                        <th>Активдүү</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rows as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->last_name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->getRoleNames()->first()}}</td>
                                        <td>{!!$item->active!!}</td>
                                        <td>
                                            <a href="{{route('admin.user.edit', $item->id)}}" class="btn btn-warning">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            @if(auth()->user()->id != $item->id)
                                            <a onclick="return confirm('{{trans('t.confirm_delete')}}')"
                                                href="{{route('admin.user.delete', $item->id)}}" class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Аты</th>
                                        <th>Фамилиясы</th>
                                        <th>Эл дареги</th>
                                        <th>Ролу</th>
                                        <th>Активдүү</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
