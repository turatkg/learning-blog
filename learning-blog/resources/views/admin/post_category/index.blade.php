@extends('admin.layouts.base')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{trans('t.post_categories')}}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">{{trans('t.main')}}</a></li>
              <li class="breadcrumb-item active">{{trans('t.post_categories')}}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">{{trans('t.all_post_categories')}}</h3>
                  <div class="float-right">
                      <a href="{{route('admin.post_category.create')}}" class="btn btn-primary">{{trans('t.post_category_create')}}</a>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>{{trans('t.title')}}</th>
                      <th>{{trans('t.posts_amount')}}</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($rows as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->posts->count()}}</td>
                            <td>
                                <a href="{{route('admin.post_category.edit', $item->id)}}" class="btn btn-warning">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a onclick="return confirm('{{trans('t.confirm_delete')}}')" href="{{route('admin.post_category.delete', $item->id)}}" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>{{trans('t.title')}}</th>
                      <th>{{trans('t.posts_amount')}}</th>
                      <th></th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
