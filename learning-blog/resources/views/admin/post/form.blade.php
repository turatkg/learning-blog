<div class="card-body">
    <div class="form-group">
        <label>{{trans('t.title')}}</label>
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>Түшүндүрмө</label>
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>Мазмуну</label>
        {!! Form::textarea('content', null, ['class' => 'form-control editor', 'rows' => 15]) !!}
    </div>
    <div class="form-group">
        <label>Тегдер</label>
        {!! Form::text('tags', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>Категориясы</label>
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label for="exampleInputFile">{{trans('t.image')}} </label>
        {!! Form::file('image', ['id' => 'exampleInputFile', 'accept' => 'image/*']) !!}
    <div class="form-check">
        {!! Form::hidden('is_editors_pick', 0) !!}
        {!! Form::checkbox('is_editors_pick', 1, null, ['class' => 'form-check-input', 'id' => 'editorspick']) !!}
        <label class="form-check-label" for="editorspick">{{trans('t.is_editors_pick')}} </label>
    </div>
    <div class="form-check">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', 1, null, ['class' => 'form-check-input', 'id' => 'exampleCheck1']) !!}
        <label class="form-check-label" for="exampleCheck1">Активдүү</label>
    </div>
</div>
<!-- /.card-body -->

<div class="card-footer">
    <button type="submit" class="btn btn-success">Сактоо</button>
</div>
