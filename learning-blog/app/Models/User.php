<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullName()
    {
        return "{$this->name} {$this->last_name}";
    }

    public function getImageAttribute($value)
    {
        if($value != null)
        {
            return $value;
        }
        return '/assets/images/user.png';
    }

    public function getActiveAttribute($value)
    {
        if($value)
        {
            return "<i style='color:green' class='fa fa-check-circle'></i>";
        }
        return "<i style='color:red' class='fa fa-times-circle'></i>";
    }
}
