<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = [
        'value',
        'note',
        'post_id',
        'user_id',
    ];

    public static function boot() {
	    parent::boot();

	    static::creating(function($item) {
            if(auth()->user()){
                $item->user_id = auth()->user()->id;
            }
	    });
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
