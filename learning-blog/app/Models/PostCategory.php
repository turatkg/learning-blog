<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    protected $fillable = [
        'title',
        'description',
        'is_important',
        'active',
    ];

    public $timestamps = false;

    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id');
    }

}
