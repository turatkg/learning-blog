<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PostCategory;
use Illuminate\Http\Request;

class PostCategoryController extends Controller
{
    public function index()
    {
        $rows = PostCategory::all();
        return view('admin.post_category.index', compact('rows'));
    }

    public function create()
    {
        $row = new PostCategory;
        return view('admin.post_category.create', compact('row'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        PostCategory::create($request->except('image'));

        return redirect()->route('admin.post_category.index');
    }

    public function edit($id)
    {
        $row = PostCategory::findOrFail($id);
        return view('admin.post_category.edit', compact('row'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $post = PostCategory::findOrFail($id);
        $post->update($request->except('image'));

        return redirect()->route('admin.post_category.index');
    }

    public function delete($id){
        $post = PostCategory::findOrFail($id);
        $post->delete();
        return redirect()->back();
    }
}
