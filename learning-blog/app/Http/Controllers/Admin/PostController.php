<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $rows = Post::all();
        return view('admin.post.index', compact('rows'));
    }

    public function create()
    {
        $row = new Post;
        $categories = PostCategory::all()->pluck('title', 'id');
        return view('admin.post.create', compact('row', 'categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);
        $row = Post::create($request->except('image'));

        if($request->hasFile('image')){
            $row->storeImage($request->file('image'));
        }

        return redirect()->route('admin.post.index');
    }

    public function edit($id)
    {
        $row = Post::findOrFail($id);
        $categories = PostCategory::all()->pluck('title', 'id');
        return view('admin.post.edit', compact('row', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);
        $row = Post::findOrFail($id);
        if($request->hasFile('image')){
            $row->storeImage($request->file('image'));
        }
        $row->update($request->except('image'));
        return redirect()->route('admin.post.index');
    }

    public function delete($id){
        $post = Post::findOrFail($id);
        $post->delete();
        return redirect()->back();
    }
}
