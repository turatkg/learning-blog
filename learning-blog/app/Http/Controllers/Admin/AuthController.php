<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;

class AuthController extends Controller
{
    public function loginForm()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        $result = Auth::attempt($credentials);
        if($result){
            if(Auth::user()->active){
                return redirect()->route('admin.index');
            }else{
                Auth::logout();
                return redirect()->back()->with('message', trans('t.you_are_not_activated_yet'));
            }
        }
        return redirect()->back()->with('message', trans('t.incorrect_password_or_email'));
    }

    public function registerForm()
    {
        return view('admin.auth.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'last_name' => 'nullable',
            'password' => 'required|confirmed',
            'image' => 'nullable|mimes:jpeg,jpg,png|max:10000',
        ]);

        $fieldValues = $request->all();
        $password = $request->get('password');
        $fieldValues['password'] = Hash::make($password);
        $row = User::create($fieldValues);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $folder = "assets/uploads/users/{$row->id}";
            if(!file_exists($folder))
            {
                mkdir($folder, 0777, true);
            }

            $image_name = "user_image.jpg";

            $image_path = "{$folder}/{$image_name}";

            Image::make($file)->fit(500, 500)->encode('jpg')->save($image_path);

            $row->image = $image_path;
            $row->save();
        }

        return redirect()->route('login')->with('message_success', trans('t.successfully_registered'));
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('login');
    }
}
