<?php

namespace App\Http\Middleware;

use App\Models\Contact;
use App\Models\PostCategory;
use Closure;
use Illuminate\Support\Facades\View;

class ViewVariablesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $categories = PostCategory::where('active', true)->where('is_important', true)->get();
        View::share('nav_categories', $categories);

        $contacts = Contact::where('active', true)->get();
        View::share('nav_contacts', $contacts);
        return $next($request);
    }
}
