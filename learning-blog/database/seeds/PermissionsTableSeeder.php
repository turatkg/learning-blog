<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id' => 1,
            'name' => 'Administrator',
        ]);
        Role::create([
            'id' => 2,
            'name' => 'Editor',
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => '1',
            'model_type' => 'App\Models\User',
            'model_id' => '1'
            ]);

        // DB::table('model_has_roles')->insert([
        //     'role_id' => '2',
        //     'model_type' => 'App\Models\User',
        //     'model_id' => '6'
        // ]);
    }
}
