<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Erkebek',
            'email' => 'erkebek.ab@gmail.com',
            'password' => Hash::make('erke123'),
            'active' => true,
        ]);
    }
}
